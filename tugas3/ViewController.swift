//
//  ViewController.swift
//  tugas3
//
//  Created by Caroline Chan on 21/10/22.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor.black
    }
    
    @IBAction func greenButtonTapped(_ sender: Any) {
        let sender: [String: Any] = ["name": "green", "color": UIColor.systemGreen]
        performSegue(withIdentifier: "showPageB", sender: sender)
    }
    
    @IBAction func blueButtonTapped(_ sender: Any) {
        let sender: [String: Any] = ["name": "blue", "color": UIColor.systemBlue]
        performSegue(withIdentifier: "showPageB", sender: sender)
    }
    
    @IBAction func redButtonTapped(_ sender: Any) {
        let sender: [String: Any] = ["name": "red", "color": UIColor.systemRed]
        performSegue(withIdentifier: "showPageB", sender: sender)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPageB" {
            let secondView = segue.destination as! BViewController
            let object = sender as! [String: Any]
            secondView.color = object["color"] as? UIColor
            secondView.name = object["name"] as? String
        }
    }
}

