//
//  CViewController.swift
//  tugas3
//
//  Created by Caroline Chan on 22/10/22.
//

import UIKit

class CViewController: UIViewController {

    var color: UIColor?
    var name: String?
    
    @IBOutlet weak var colorLabelView: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = color
        
        colorLabelView.text = "It's " + name!
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}
