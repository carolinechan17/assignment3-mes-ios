//
//  BViewController.swift
//  tugas3
//
//  Created by Caroline Chan on 22/10/22.
//

import UIKit

class BViewController: UIViewController {

    var color: UIColor?
    var name: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = color
        print(name!)
    }
    
    @IBAction func showCButtonTapped(_ sender: Any) {
        let sentName: String = self.name ?? ""
        let sentColor: UIColor = self.color ?? UIColor.white
        let sender: [String: Any] = ["name": sentName, "color": sentColor]
        performSegue(withIdentifier: "showPageC", sender: sender)
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPageC" {
            let thirdView = segue.destination as! CViewController
            let object = sender as! [String: Any]
            thirdView.color = object["color"] as? UIColor
            thirdView.name = (object["name"] as? String)!
        }
    }
    
}
